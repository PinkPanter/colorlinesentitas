//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentLookupGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public static class GameComponentsLookup {

    public const int AnyGemListener = 0;
    public const int Board = 1;
    public const int BoardFillness = 2;
    public const int GameScore = 3;
    public const int GameScoreListener = 4;
    public const int GameState = 5;
    public const int GameStateListener = 6;
    public const int Gem = 7;
    public const int GemPromice = 8;
    public const int MoveTarget = 9;
    public const int Path = 10;
    public const int PathListener = 11;
    public const int PositionOnBoard = 12;
    public const int PositionOnBoardListener = 13;
    public const int Selected = 14;
    public const int SelectedListener = 15;
    public const int SelectedRemovedListener = 16;

    public const int TotalComponents = 17;

    public static readonly string[] componentNames = {
        "AnyGemListener",
        "Board",
        "BoardFillness",
        "GameScore",
        "GameScoreListener",
        "GameState",
        "GameStateListener",
        "Gem",
        "GemPromice",
        "MoveTarget",
        "Path",
        "PathListener",
        "PositionOnBoard",
        "PositionOnBoardListener",
        "Selected",
        "SelectedListener",
        "SelectedRemovedListener"
    };

    public static readonly System.Type[] componentTypes = {
        typeof(AnyGemListenerComponent),
        typeof(BoardComponent),
        typeof(BoardFillnessComponent),
        typeof(GameScoreComponent),
        typeof(GameScoreListenerComponent),
        typeof(GameStateComponent),
        typeof(GameStateListenerComponent),
        typeof(GemComponent),
        typeof(GemPromice),
        typeof(MoveTargetComponent),
        typeof(PathComponent),
        typeof(PathListenerComponent),
        typeof(PositionOnBoardComponent),
        typeof(PositionOnBoardListenerComponent),
        typeof(SelectedComponent),
        typeof(SelectedListenerComponent),
        typeof(SelectedRemovedListenerComponent)
    };
}
