﻿using UnityEngine;
using UnityEngine.UI;

namespace Visual
{
    public class BoardVisualController : MonoBehaviour
    {
        [SerializeField]
        private RectTransform board;
        
        
        [SerializeField]
        private GameObject horizontalLinePrefab;
        [SerializeField]
        private GameObject verticalLinePrefab;
        
        [SerializeField]
        private Transform horizontalContainer;
        [SerializeField]
        private Transform verticalContainer;
        [SerializeField]
        private GridLayoutGroup grid;
        [SerializeField]
        private RectTransform spawnGrid;
        [SerializeField]
        private GridElement gridElementPrefab;
        
        void Start()
        {
            if (Contexts.sharedInstance.configs.hasBoardConfig)
                SetupBoard(Contexts.sharedInstance.configs.boardConfig.value.boardSize);
        }

        private void SetupBoard(Vector2Int boardSize)
        {
            var size = board.rect.size;
            grid.cellSize = new Vector2(
                (size.x - horizontalLinePrefab.GetComponent<RectTransform>().rect.size.x * (boardSize.x - 1)) / boardSize.x,
                (size.y - verticalLinePrefab.GetComponent<RectTransform>().rect.size.y * (boardSize.y - 1)) / boardSize.y);

            spawnGrid.sizeDelta = new Vector2(spawnGrid.sizeDelta.x, grid.cellSize.y);
            for (int y = 0; y < boardSize.y; y++)
            {
                for (int x = 0; x < boardSize.x; x++)
                {
                    var rectTransform = (RectTransform) Instantiate(gridElementPrefab).transform;
                    rectTransform.gameObject.name = $"{x}:{y}";
                    rectTransform.SetParent(grid.transform, false);
                    rectTransform.localScale = Vector3.one;
                }
            }

            SetupLines(horizontalLinePrefab, horizontalContainer, boardSize.x);
            SetupLines(verticalLinePrefab, verticalContainer, boardSize.y);
        }

        private void SetupLines(GameObject prefab, Transform container, int count)
        {
            for (int i = 1; i < container.childCount; i++)
            {
                DestroyImmediate(container.GetChild(i).gameObject);
                i--;
            }

            for (int i = 0; i < count - 1; i++)
            {
                Instantiate(prefab, container).SetActive(true);
            }
        }
    }
}
