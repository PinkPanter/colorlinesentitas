﻿using System.Collections.Generic;
using DG.Tweening;
using Entitas;
using UnityEngine;
using UnityEngine.UI;
using Visual.Views;

namespace Visual
{
    public class GemVisualController : MonoBehaviour, IPositionOnBoardListener, IPathListener
    {
        [SerializeField]
        private GemView gemsPrefab;
        [SerializeField]
        private LayoutGroup spawnContainer;
        [SerializeField]
        private Transform gridContainer;
    
        private IGroup<GameEntity> gemsEnetities;
        private Dictionary<GameEntity, GemView> created = new Dictionary<GameEntity, GemView>();

        void OnEnable()
        {
            gemsEnetities =
                GameController.SessionContext.game.GetGroup(GameMatcher.Gem);
        
            gemsEnetities.OnEntityAdded += GemsEntitiesOnEntityAdded;
            gemsEnetities.OnEntityRemoved += GemsEntitiesOnEntityRemoved;

            var existingEntities = gemsEnetities.GetEntities();
            foreach (var entity in existingEntities)
                CreateGem(entity);
        }

        private void OnDisable()
        {
            gemsEnetities.OnEntityAdded -= GemsEntitiesOnEntityAdded;
            gemsEnetities.OnEntityRemoved -= GemsEntitiesOnEntityRemoved;
        }
    
        private void GemsEntitiesOnEntityAdded(IGroup<GameEntity> group, GameEntity entity, int index, IComponent component)
        {
            CreateGem(entity);
            if (entity.hasPositionOnBoard)
                OnPositionOnBoard(entity, entity.positionOnBoard.position);

        }

        private void GemsEntitiesOnEntityRemoved(IGroup<GameEntity> group, GameEntity entity, int index, IComponent component)
        {
            RemoveGem(entity);
        }

        public void OnPositionOnBoard(GameEntity entity, Vector2Int position)
        {
            var target = (RectTransform)gridContainer.GetChild(ToGridPosition(position));

            if (created[entity].RectTransfrom.parent == spawnContainer.transform)
            {
                created[entity].gameObject.AddComponent<LayoutElement>().ignoreLayout = true;

                created[entity].RectTransfrom.DOMove(target.position, 0.5f).OnComplete(() =>
                {
                    created[entity].RectTransfrom.SetParent(target, true);
                    created[entity].RectTransfrom.anchorMin = Vector2.zero;
                    created[entity].RectTransfrom.anchorMax = Vector2.one;

                    created[entity].RectTransfrom.anchoredPosition = Vector2.zero;
                    created[entity].RectTransfrom.sizeDelta = Vector2.zero;
                });
            }
            else
            {
                created[entity].RectTransfrom.SetParent(target, true);
                created[entity].RectTransfrom.anchorMin = Vector2.zero;
                created[entity].RectTransfrom.anchorMax = Vector2.one;

                created[entity].RectTransfrom.anchoredPosition = Vector2.zero;
                created[entity].RectTransfrom.sizeDelta = Vector2.zero;
            }
        }


        public void OnPath(GameEntity entity, Vector2Int[] targets, int step)
        {
            Vector3[] path = new Vector3[targets.Length];
            for (int i = 0; i < path.Length; i++)
            {
                path[i] = gridContainer.GetChild(ToGridPosition(targets[i])).position;
            }

            var entTransfrom = created[entity].RectTransfrom;
            var lastParent = gridContainer.GetChild(ToGridPosition(targets[targets.Length - 1]));

            entTransfrom.DOPath(path, 0.5f).OnComplete(() =>
            {
                entity.ReplacePositionOnBoard(targets[targets.Length - 1]);
                entity.RemovePath();
            });
        }


        private void CreateGem(GameEntity gemEntity)
        {
            var gemView = Instantiate(gemsPrefab.gameObject, spawnContainer.transform).GetComponent<GemView>();
       
            spawnContainer.CalculateLayoutInputHorizontal();
            spawnContainer.SetLayoutHorizontal();
       
            gemEntity.AddPositionOnBoardListener(this);
            gemEntity.AddPathListener(this);
       
            gemView.Init(gemEntity);
            created.Add(gemEntity, gemView);
        }
    
        private void RemoveGem(GameEntity gemEntity)
        {
            if (created.ContainsKey(gemEntity))
            {
                created[gemEntity].DestroyView();
                created.Remove(gemEntity);
            }
        }

        private int ToGridPosition(Vector2Int position)
        {
            var size = Contexts.sharedInstance.configs.boardConfig.value.boardSize;
            return position.x + size.x * position.y;
        }
    }
}
