﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Visual
{
    public class GridElement : MonoBehaviour, IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData)
        {
            var ent = GameController.SessionContext.input.CreateEntity();
            ent.AddGridClick(GetGridPosition(Contexts.sharedInstance.configs.boardConfig.value.boardSize));
        }

        private Vector2Int GetGridPosition(Vector2Int size)
        {
            var current = transform.GetSiblingIndex();
            var yPos = current / size.x;
            return new Vector2Int(current - size.x * yPos, current / size.x);
        }
    }
}
