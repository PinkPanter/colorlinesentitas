﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Visual.Views
{
    public class GameOverView : MonoBehaviour, IGameStateListener
    {
        [SerializeField]
        private GameObject holder;
    
        public void Start()
        {
            GameController.SessionContext.game.gameStateEntity.AddGameStateListener(this);
        }

        public void OnGameState(GameEntity entity, GameStateComponent.GameState state)
        {
            holder.SetActive(state == GameStateComponent.GameState.GameOver);
        }

        public void Reload()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
