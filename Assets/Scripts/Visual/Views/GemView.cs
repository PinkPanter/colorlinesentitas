﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Visual.Views
{
    public class GemView : MonoBehaviour, ISelectedListener, ISelectedRemovedListener
    {
        public RectTransform RectTransfrom
        {
            get
            {
                if (rectTransform == null)
                    rectTransform = GetComponent<RectTransform>();

                return rectTransform;
            }
        }

        [SerializeField]
        private Image image;

        [SerializeField]
        private Color[] typeColors;

        private GameEntity gemEntity;
        private RectTransform rectTransform;
    
        public void Init(GameEntity entity)
        {
            gemEntity = entity;
            gemEntity.AddSelectedListener(this);
            gemEntity.AddSelectedRemovedListener(this);
            if (typeColors.Length <= entity.gem.type)
                Debug.LogError($"Couldn't get color for {entity.gem.type}");
            else
                image.color = typeColors[entity.gem.type];
        }

        public void DestroyView()
        {
            const float destroyTime = 0.3f;
            image.DOFade(0, destroyTime);
            Destroy(gameObject, destroyTime);
        }

        public void OnSelected(GameEntity entity)
        {
            transform.DORewind();
            transform.DOScale(Vector3.one * 1.1f, 0.5f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InBounce);
        }

        public void OnSelectedRemoved(GameEntity entity)
        {
            transform.DORewind();
        }
    }
}
