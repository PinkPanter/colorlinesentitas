﻿using TMPro;
using UnityEngine;

namespace Visual.Views
{
    public class ScoreView : MonoBehaviour, IGameScoreListener
    {
        [SerializeField]
        public string scoreText = "Score: ";
        
        [SerializeField]
        private TextMeshProUGUI scoreTMP;

        private void Start()
        {
            GameController.SessionContext.game.gameScoreEntity.AddGameScoreListener(this);
            OnGameScore(GameController.SessionContext.game.gameScoreEntity, GameController.SessionContext.game.gameScore.score);
        }
        
        public void OnGameScore(GameEntity entity, int score)
        {
            scoreTMP.text = scoreText + score.ToString();
        }
    }
}
