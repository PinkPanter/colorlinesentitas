using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Configs, Unique, ComponentName("GemsRulesConfig")]
public interface IGemsRulesConfig
{
    int initialCount { get; }
    
    int newPerTurn { get; }
    
    int types { get; }
    
    int minMatch { get; }
    
    int scorePerRemove { get; }
}
