using System;
using UnityEngine;
using Utils;

[CreateAssetMenu(menuName = "ColorLines/ScriptableBoardConfig")]
public class ScriptableBoardConfig : ConfigScriptableObject<ScriptableBoardConfig.SerializableBoardConfig>, IBoardConfig
{
    public static ScriptableBoardConfig Default =>
        GetInstance<ScriptableBoardConfig, SerializableBoardConfig>();
    
    public Vector2Int boardSize => data.boardSize;
    
    [Serializable]
    public class SerializableBoardConfig
    {
        public Vector2Int boardSize = new Vector2Int(9, 9);
    }
}
