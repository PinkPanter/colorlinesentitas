using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Configs, Unique, ComponentName("BoardConfig")]
public interface IBoardConfig
{
    Vector2Int boardSize { get; }
}
