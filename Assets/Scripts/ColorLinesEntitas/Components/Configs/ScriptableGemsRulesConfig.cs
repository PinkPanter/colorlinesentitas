using System;
using UnityEngine;
using Utils;

[CreateAssetMenu(menuName = "ColorLines/SerializableGemsRulesConfig")]
public class ScriptableGemsRulesConfig : ConfigScriptableObject<ScriptableGemsRulesConfig.SerializableGemsRulesConfig>, IGemsRulesConfig
{
    public static ScriptableGemsRulesConfig Default =>
        GetInstance<ScriptableGemsRulesConfig, SerializableGemsRulesConfig>();
    

    public int initialCount => data.initialCount;
    public int newPerTurn => data.newPerTurn;
    public int types => data.types;
    public int minMatch => data.minMatch;
    public int scorePerRemove => data.scorePerRemove;
    
    [Serializable]
    public class SerializableGemsRulesConfig
    {
        public int initialCount = 5;
        public int newPerTurn = 3;
        public int types = 7;
        public int minMatch = 5;
        public int scorePerRemove = 10;
    }
}
