﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Game, Event(EventTarget.Self)]
public class PathComponent : IComponent
{
    public Vector2Int[] targets;
    public int step;
}
