﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique, Event(EventTarget.Self)]
public class GameScoreComponent : IComponent
{
    public int score;
}
