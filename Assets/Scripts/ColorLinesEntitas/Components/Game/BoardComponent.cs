﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Game, Unique]
public sealed class BoardComponent : IComponent
{
    public Vector2Int size;
}

