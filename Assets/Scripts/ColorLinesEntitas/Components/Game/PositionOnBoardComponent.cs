﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Game, Event(EventTarget.Self)]
public class PositionOnBoardComponent : IComponent
{
    public Vector2Int position;
}
