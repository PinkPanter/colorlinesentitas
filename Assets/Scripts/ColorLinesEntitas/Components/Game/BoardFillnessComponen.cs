﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public class BoardFillnessComponent : IComponent
{
   public int count;
   
   public bool[,] fillness;
}
