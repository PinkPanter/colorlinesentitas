﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique, Event(EventTarget.Self, EventType.Added), Event(EventTarget.Self, EventType.Removed)]
public class SelectedComponent : IComponent
{
}
