﻿using Entitas;
using UnityEngine;

[Game]
public class MoveTargetComponent : IComponent
{
    public Vector2Int target;
}
