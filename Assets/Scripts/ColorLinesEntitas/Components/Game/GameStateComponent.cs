﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique, Event(EventTarget.Self)]
public class GameStateComponent : IComponent
{
    public GameState state;

    public enum GameState
    {
        CheckForMatchAndWaitForInput,
        WaitingForInput,
        MoveObjects,
        CheckForMatchAndSpawnGems,
        SpawnGems,
        GameOver
    }
}
