﻿using Entitas;
using UnityEngine;

[Input]
public class GridClickComponent : IComponent
{
    public Vector2Int position;
}
