﻿using Entitas;
using UnityEngine;

public partial class Contexts
{
    public const string GemIndexerName = "GemToCoords";

    [Entitas.CodeGeneration.Attributes.PostConstructor]
    public void InitializeGemEntityIndices()
    {
        game.AddEntityIndex(new PrimaryEntityIndex<GameEntity, Vector2Int>(
            GemIndexerName,
            game.GetGroup(GameMatcher
                .AllOf(GameMatcher.Gem, GameMatcher.PositionOnBoard)
            ),
            (e, c) =>
                (c as PositionOnBoardComponent)?.position ?? e.positionOnBoard.position));
    }
}


public static class GameContextExtention
{
    public static GameEntity GetGemWithPosition(this GameContext context, Vector2Int value)
    {
        return ((PrimaryEntityIndex<GameEntity, Vector2Int>) context.GetEntityIndex(Contexts.GemIndexerName))
            .GetEntity(value);
    }

    public static Vector2Int GetEmptyCell(this GameContext context, out bool cellExist)
    {
        cellExist = false;
        var size = global::Contexts.sharedInstance.configs.boardConfig.value.boardSize;

        var fillness = context.boardFillness.fillness;

        int[] freeCells = new int[size.x * size.y - context.boardFillness.count];

        if (freeCells.Length == 0)
            return new Vector2Int(0, 0);

        int index = 0;
        for (int i = 0; i < size.x; i++)
        {
            for (int j = 0; j < size.y; j++)
            {
                if (!fillness[i, j])
                {
                    freeCells[index] = j * size.x + i;
                    index++;
                }
            }
        }

        cellExist = true;
        var selected = freeCells[Random.Range(0, freeCells.Length)];
        var yPos = selected / size.x;
        return new Vector2Int(selected - size.x * yPos, selected / size.x);
    }
}