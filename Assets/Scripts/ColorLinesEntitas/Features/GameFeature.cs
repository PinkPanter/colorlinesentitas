﻿using ColorLinesEntitas.Systems.Game;
using ColorLinesEntitas.Systems.Input;

namespace ColorLinesEntitas.Features
{
    public class GameFeature : Feature
    {
        public GameFeature(Contexts contexts)
        {
            //GAME
            contexts.game.SetGameScore(0);
            
            Add(new BoardSystem(contexts));
            Add(new GemSpawnSystem(contexts));
            Add(new CheckFillnessSystem(contexts));
            Add(new SelectedPathFinderSystem(contexts));
            Add(new PathMovementCompleteSystem(contexts));
            
            //EVENTS
            Add(new GameScoreEventSystem(contexts));
            Add(new GameStateEventSystem(contexts));
            Add(new PositionOnBoardEventSystem(contexts));
            Add(new SelectedEventSystem(contexts));
            Add(new SelectedRemovedEventSystem(contexts));
            Add(new PathEventSystem(contexts));
            
            contexts.game.SetGameState(GameStateComponent.GameState.CheckForMatchAndWaitForInput);
            
            //INPUT
            Add(new ClickOnBoardSystem(contexts));
        }
    }
}
