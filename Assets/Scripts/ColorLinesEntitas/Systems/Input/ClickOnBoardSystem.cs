﻿using System.Collections.Generic;
using Entitas;

namespace ColorLinesEntitas.Systems.Input
{
    public class ClickOnBoardSystem : ReactiveSystem<InputEntity>
    {
        protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context) =>
            context.CreateCollector(new TriggerOnEvent<InputEntity>(InputMatcher.GridClick, GroupEvent.Added));

        protected override bool Filter(InputEntity entity) => entity.hasGridClick;

        private GameContext gameContext;
    
        public ClickOnBoardSystem(Contexts contexts) : base((IContext<InputEntity>) contexts.input)
        {
            gameContext = contexts.game;
        }

        protected override void Execute(List<InputEntity> entities)
        {
            if(gameContext.gameState.state != GameStateComponent.GameState.WaitingForInput)
                return;
        
            foreach (var inputEntity in entities)
            {
                var pos = inputEntity.gridClick.position;
                var gem = gameContext.GetGemWithPosition(pos);

                if (gem != null)
                {
                    if (gameContext.selectedEntity != null)
                        gameContext.selectedEntity.isSelected = false;
                
                    gem.isSelected = true;
                }
                else
                {
                    var selected = gameContext.selectedEntity;
                    if (selected != null)
                    {
                        if(selected.hasMoveTarget)
                            selected.ReplaceMoveTarget(pos);
                        else
                            selected.AddMoveTarget(pos);
                    }
                }
            
                inputEntity.Destroy();
            }
        }
    }
}
