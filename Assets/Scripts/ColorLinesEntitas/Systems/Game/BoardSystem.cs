﻿using Entitas;

namespace ColorLinesEntitas.Systems.Game
{
    public class BoardSystem : IInitializeSystem, ITearDownSystem
    {
        private GameContext gameContext;
        
        private IGroup<GameEntity> entitiesWithPositionGroup;

        public BoardSystem(Contexts contexts)
        {
            gameContext = contexts.game;
        }
    
        public void Initialize()
        {
            var boardConfig = Contexts.sharedInstance.configs.boardConfigEntity.boardConfig.value;
            gameContext.SetBoardFillness(0, new bool[boardConfig.boardSize.x, boardConfig.boardSize.y]);

            entitiesWithPositionGroup = gameContext.GetGroup(GameMatcher.PositionOnBoard);
            entitiesWithPositionGroup.OnEntityAdded += EntitiesWithPositionGroupOnEntityAdded;
            entitiesWithPositionGroup.OnEntityUpdated += EntitiesWithPositionGroupOnEntityUpdated;
            entitiesWithPositionGroup.OnEntityRemoved += EntitiesWithPositionGroupOnEntityRemoved;
        }

        private void EntitiesWithPositionGroupOnEntityAdded(IGroup<GameEntity> group, GameEntity entity, int index, IComponent component)
        {
            var size = Contexts.sharedInstance.configs.boardConfig.value.boardSize;
            if (++gameContext.boardFillness.count >= size.x * size.y)
            {
                gameContext.ReplaceGameState(GameStateComponent.GameState.GameOver);
                return;
            }

            var pos = ((PositionOnBoardComponent)component).position;
            gameContext.boardFillness.fillness[pos.x, pos.y] = true;
        }

        private void EntitiesWithPositionGroupOnEntityUpdated(IGroup<GameEntity> @group, GameEntity entity, int index, IComponent previouscomponent, IComponent newcomponent)
        {
            var realComp = (PositionOnBoardComponent) newcomponent;
            gameContext.boardFillness.fillness[realComp.position.x, realComp.position.y] = true;
            realComp = (PositionOnBoardComponent) previouscomponent;
            gameContext.boardFillness.fillness[realComp.position.x, realComp.position.y] = false;
        }
    
        private void EntitiesWithPositionGroupOnEntityRemoved(IGroup<GameEntity> @group, GameEntity entity, int index, IComponent component)
        {
            var pos = ((PositionOnBoardComponent)component).position;
            gameContext.boardFillness.count--;
            gameContext.boardFillness.fillness[pos.x, pos.y] = false;
        }
  
        //TEARDOWN?
        public void TearDown()
        {
            gameContext.RemoveBoardFillness();
        
            entitiesWithPositionGroup.OnEntityAdded -= EntitiesWithPositionGroupOnEntityAdded;
            entitiesWithPositionGroup.OnEntityUpdated -= EntitiesWithPositionGroupOnEntityUpdated;
            entitiesWithPositionGroup.OnEntityRemoved -= EntitiesWithPositionGroupOnEntityRemoved; 
        }
    }
}
