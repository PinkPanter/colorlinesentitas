﻿using System.Collections.Generic;
using AStar;
using Entitas;
using UnityEngine;

namespace ColorLinesEntitas.Systems.Game
{
    public class SelectedPathFinderSystem : ReactiveSystem<GameEntity>, IInitializeSystem
    {
        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
            context.CreateCollector(new TriggerOnEvent<GameEntity>(GameMatcher.MoveTarget, GroupEvent.Added));

        protected override bool Filter(GameEntity entity) => entity.hasMoveTarget && entity.isSelected && entity.hasPositionOnBoard;

        private GameContext gameContext;
        private PathFinder pathFinder;
        
        public SelectedPathFinderSystem(Contexts contexts) : base((IContext<GameEntity>) contexts.game)
        {
            gameContext = contexts.game;
        }

        public void Initialize()
        {
            var grid = gameContext.boardFillness.fillness;
            pathFinder = new PathFinder(grid, new PathFinderOptions() {Diagonals = false});
        }
        
        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var entity in entities)
            {
                var startPos = entity.positionOnBoard.position;
                var endPos = entity.moveTarget.target;

                var path = pathFinder.FindPath(new Point(startPos.x, startPos.y), new Point(endPos.x, endPos.y));
                if (path != null)
                {
                    Vector2Int[] pathArray = new Vector2Int[path.Count - 1];
                    for (int i = 0; i < pathArray.Length; i++)
                    {
                        pathArray[i] = path[path.Count - i - 2].Vector2Int;
                    }

                    entity.AddPath(pathArray, 0);
                    gameContext.gameState.state = GameStateComponent.GameState.MoveObjects;
                    
                    entity.isSelected = false;
                }

                entity.RemoveMoveTarget();
            }
        }
    }
}
