﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace ColorLinesEntitas.Systems.Game
{
    public class CheckFillnessSystem : ReactiveSystem<GameEntity>
    {
        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
            context.CreateCollector(GameMatcher.GameState);

        protected override bool Filter(GameEntity entity) =>
            entity.gameState.state == GameStateComponent.GameState.CheckForMatchAndSpawnGems ||
            entity.gameState.state == GameStateComponent.GameState.CheckForMatchAndWaitForInput;

        private CheckAlgorithm[] checkAlgorithms;
        
        private GameContext gameContext;
    
        public CheckFillnessSystem(Contexts contexts) : base((IContext<GameEntity>) contexts.game)
        {
            gameContext = contexts.game;
            
            checkAlgorithms = new CheckAlgorithm[4];
            var boardSetting = Contexts.sharedInstance.configs.boardConfig.value;
            var gemSettings = Contexts.sharedInstance.configs.gemsRulesConfig.value;
            
            checkAlgorithms[0] = new CheckAlgorithm(gameContext, new Vector2Int(0, 0), gemSettings.minMatch)
            {
                //HorizontalCheck
                GetNextElementImpl = (Vector2Int current, int step, out bool isEnd) =>
                {
                    current += new Vector2Int(step, 0);
                    isEnd = current.x >= boardSetting.boardSize.x;
                    return current;
                },
                GetNextInitialElementImpl = (Vector2Int currentInitialElement, out bool isEnd) =>
                {
                    currentInitialElement += new Vector2Int(0, 1);
                    isEnd = currentInitialElement.y >= boardSetting.boardSize.y;
                    return currentInitialElement;
                }
            };
            
            checkAlgorithms[1] = new CheckAlgorithm(gameContext, new Vector2Int(0, 0), gemSettings.minMatch)
            {
                //VerticalCheck
                GetNextElementImpl = (Vector2Int current, int step, out bool isEnd) =>
                {
                    current += new Vector2Int(0, step);
                    isEnd = current.y >= boardSetting.boardSize.y;
                    return current;
                },
                GetNextInitialElementImpl = (Vector2Int currentInitialElement, out bool isEnd) =>
                {
                    currentInitialElement += new Vector2Int(1, 0);
                    isEnd = currentInitialElement.x >= boardSetting.boardSize.x;
                    return currentInitialElement;
                }
            };
            
            checkAlgorithms[2] = new CheckAlgorithm(gameContext, new Vector2Int(0, 0), gemSettings.minMatch)
            {
                //DiagonalTopRightCheck
                GetNextElementImpl = (Vector2Int current, int step, out bool isEnd) =>
                {
                    current += new Vector2Int(step, -step);
                    isEnd = current.x >= boardSetting.boardSize.x || current.y < 0;
                    return current;
                },
                GetNextInitialElementImpl = (Vector2Int currentInitialElement, out bool isEnd) =>
                {
                    if(currentInitialElement.y < boardSetting.boardSize.y - 1)
                        currentInitialElement += new Vector2Int(0, 1);
                    else
                        currentInitialElement += new Vector2Int(1, 0);
                    
                    isEnd = currentInitialElement.x >= boardSetting.boardSize.x;
                    return currentInitialElement;
                }
            };
            
            checkAlgorithms[3] = new CheckAlgorithm(gameContext, new Vector2Int(0, boardSetting.boardSize.x - 1), gemSettings.minMatch)
            {
                //DiagonalBottomLeft
                GetNextElementImpl = (Vector2Int current, int step, out bool isEnd) =>
                {
                    current += new Vector2Int(-step, -step);
                    isEnd = current.x < 0 || current.y < 0;
                    return current;
                },
                GetNextInitialElementImpl = (Vector2Int currentInitialElement, out bool isEnd) =>
                {
                    if(currentInitialElement.x < boardSetting.boardSize.x - 1)
                        currentInitialElement += new Vector2Int(1, 0);
                    else
                        currentInitialElement += new Vector2Int(0, -1);
                    
                    isEnd = currentInitialElement.y < 0;
                    return currentInitialElement;
                }
            };
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var gameEntity in entities)
            {
                CheckGroup();
                if (gameEntity.gameState.state == GameStateComponent.GameState.CheckForMatchAndSpawnGems)
                {
                    gameEntity.ReplaceGameState(GameStateComponent.GameState.SpawnGems);
                }
                else
                {
                    gameEntity.ReplaceGameState(GameStateComponent.GameState.WaitingForInput);
                }
            }
        }

        private void CheckGroup()
        {
            var fillness = gameContext.boardFillness.fillness;

            foreach (var checkAlgorithm in checkAlgorithms)
            {
                checkAlgorithm?.Check(fillness);
            }
        }

        private class CheckAlgorithm
        {
            public delegate Vector2Int GetNextInitialElement(Vector2Int currentInitialElement, out bool isEnd);

            public delegate Vector2Int GetNextElement(Vector2Int current, int step, out bool isEnd);

            
            public GetNextInitialElement GetNextInitialElementImpl;
            public GetNextElement GetNextElementImpl;

            private Vector2Int initialElement;
            private int minMatchCount;
            private GameContext gameContext;

            public CheckAlgorithm(GameContext gameContext, Vector2Int initialElement, int minMatchCount)
            {
                this.gameContext = gameContext;
                this.initialElement = initialElement;
                this.minMatchCount = minMatchCount;
            }
            
            public void Check(bool[,] fillness)
            {
                bool isEnd = false;

                var currentInitialElement = initialElement;
                while (!isEnd)
                {
                    bool sequenceEnd = false;
                    var currentInSequence = currentInitialElement;
                    int currentType = 0;
                    int currentCount = 0;
                    
                    while (!sequenceEnd)
                    {
                        int countToRemove = 0;
                        if (fillness[currentInSequence.x, currentInSequence.y])
                        {
                            var currentElement = gameContext.GetGemWithPosition(currentInSequence);
                            if (currentCount == 0)
                                currentType = currentElement.gem.type;

                            if (currentElement.gem.type == currentType)
                            {
                                currentCount++;
                            }
                            else
                            {
                                if (currentCount >= minMatchCount)
                                    countToRemove = currentCount;

                                currentCount = 1;
                                currentType = currentElement.gem.type;
                            }
                        }
                        else
                        {
                            if (currentCount >= minMatchCount)
                                countToRemove = currentCount;
                            
                            currentCount = 0;
                        }

                        if (countToRemove > 0)
                        {
                            for (int i = 1; i <= countToRemove; i++)
                            {
                                var prev = GetNextElementImpl(currentInSequence, -i, out sequenceEnd);
                                if (!sequenceEnd)
                                {
                                    var prevElement = gameContext.GetGemWithPosition(prev);
                                    gameContext.ReplaceGameScore(gameContext.gameScore.score + Contexts.sharedInstance.configs.gemsRulesConfig.value.scorePerRemove);
                                    prevElement.Destroy();
                                }
                            }
                        }
                        currentInSequence = GetNextElementImpl(currentInSequence, 1, out sequenceEnd);
                    }
                    
                    if (currentCount >= minMatchCount)
                    {
                        for (int i = 1; i <= currentCount; i++)
                        {
                            var prev = GetNextElementImpl(currentInSequence, -i, out sequenceEnd);
                            if (!sequenceEnd)
                            {
                                var prevElement = gameContext.GetGemWithPosition(prev);
                                gameContext.ReplaceGameScore(gameContext.gameScore.score + Contexts.sharedInstance.configs.gemsRulesConfig.value.scorePerRemove);
                                prevElement.Destroy();
                            }
                        }
                    }
                    
                    currentInitialElement = GetNextInitialElementImpl(currentInitialElement, out isEnd);
                }
            }
        }
    }
}
