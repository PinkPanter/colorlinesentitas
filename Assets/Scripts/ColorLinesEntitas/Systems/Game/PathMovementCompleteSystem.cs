﻿using System.Collections.Generic;
using Entitas;

namespace ColorLinesEntitas.Systems.Game
{
    public class PathMovementCompleteSystem : ReactiveSystem<GameEntity>
    {
        private readonly GameContext gameContext;
    
        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
            context.CreateCollector(new TriggerOnEvent<GameEntity>(GameMatcher.Path, GroupEvent.Removed));

        protected override bool Filter(GameEntity entity) => true;
    
        public PathMovementCompleteSystem(Contexts context) : base((IContext<GameEntity>) context.game)
        {
            gameContext = context.game;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var gameEntity in entities)
            {
                gameContext.ReplaceGameState(GameStateComponent.GameState.CheckForMatchAndSpawnGems);
            }
        }
    }
}
