﻿using System.Collections.Generic;
using Entitas;

namespace ColorLinesEntitas.Systems.Game
{
    public class ScoreEventSystem : ReactiveSystem<GameEntity>
    {
        private readonly IContext<GameEntity> context;
    
        public ScoreEventSystem(IContext<GameEntity> context) : base(context)
        {
            this.context = context;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
            context.CreateCollector(Matcher<GameEntity>.AllOf(GameMatcher.GameScore,
                GameMatcher.GameScoreListener));

        protected override bool Filter(GameEntity entity) =>
            entity.hasGameScore && entity.hasGameScoreListener;

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var gameEntity in entities)
            {
                foreach (var listener in gameEntity.gameScoreListener.value)
                {
                    listener.OnGameScore(gameEntity, gameEntity.gameScore.score);
                }
            }
        }
    }
}
