﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace ColorLinesEntitas.Systems.Game
{
    public class GemSpawnSystem : ReactiveSystem<GameEntity>, IInitializeSystem
    {
        private readonly GameContext gameContext;

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) =>
            context.CreateCollector(GameMatcher.GameState);

        protected override bool Filter(GameEntity entity) => entity.hasGameState && entity.gameState.state == GameStateComponent.GameState.SpawnGems;

        protected IGroup<GameEntity> promises;

        public GemSpawnSystem(Contexts contexts) : base((IContext<GameEntity>) contexts.game)
        {
            gameContext = contexts.game;
            promises = gameContext.GetGroup(GameMatcher.GemPromice);
        }

        public void Initialize()
        {
            var config = Contexts.sharedInstance.configs.gemsRulesConfig.value;

            for (int i = 0; i < config.initialCount; i++)
            {
                CreateRandomGemInCell(config.types);
            }

            for (int i = 0; i < config.newPerTurn; i++)
            {
                CreateRandomGemPromise(config.types);
            }
        }

        private void CreateRandomGemInCell(int typesCount)
        {
            var gemEntity = gameContext.CreateEntity();
            gemEntity.AddGem(Random.Range(0, typesCount));

            var pos = gameContext.GetEmptyCell(out var cellExist);
            gemEntity.AddPositionOnBoard(pos);
        }

        private void CreateRandomGemPromise(int typesCount)
        {
            var gemEntity = gameContext.CreateEntity();
            gemEntity.AddGem(Random.Range(0, typesCount));
            gemEntity.isGemPromice = true;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var gameEntity in entities)
            {
                if (gameEntity.gameState.state == GameStateComponent.GameState.SpawnGems)
                {
                    var promEnt = promises.GetEntities();
                    foreach (var promise in promEnt)
                    {
                        promise.isGemPromice = false;
                    
                        var pos = gameContext.GetEmptyCell(out var cellExist);
                        if (!cellExist)
                        {
                            gameContext.ReplaceGameState(GameStateComponent.GameState.GameOver);
                            return;
                        }
                        promise.AddPositionOnBoard(pos);
                    }

                    var config = Contexts.sharedInstance.configs.gemsRulesConfig.value;
                    for (int i = 0; i < config.newPerTurn; i++)
                    {
                        CreateRandomGemPromise(Random.Range((int) 0, (int) config.types));
                    }
                }

                gameEntity.ReplaceGameState(GameStateComponent.GameState.CheckForMatchAndWaitForInput);
            }
        }
    }
}
