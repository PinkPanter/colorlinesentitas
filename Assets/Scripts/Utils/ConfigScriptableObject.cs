﻿using System.IO;
using UnityEngine;

namespace Utils
{
    public class ConfigScriptableObject<T> : ScriptableObject where T : class, new()
    {
        // ReSharper disable once InconsistentNaming
        private const string CONFIG_FOLDER = "Configs";
    
        [SerializeField]
        protected T data = new T();

        protected static T GetInstance<T, T1>() 
            where T : ConfigScriptableObject<T1>
            where T1 : class, new()
        {
            T instance;
            #if !UNITY_WEBGL
            var path = Path.Combine(Application.persistentDataPath, configFolder);
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path = Path.Combine(path, typeof(T1).Name);

            if (File.Exists(path))
            {
                instance = CreateInstance<T>();
                instance.data = JsonUtility.FromJson<T1>(File.ReadAllText(path));
            }
            else
            #endif
            {
                instance = Resources.Load<T>(Path.Combine(CONFIG_FOLDER, typeof(T1).Name));
                if (instance == null)
                {
                    instance = CreateInstance<T>();
                    instance.data = new T1();
                }

                #if !UNITY_WEBGL
                File.WriteAllText(path, JsonUtility.ToJson(instance.data));
                #endif
            }

            return instance;
        }
    }
}
