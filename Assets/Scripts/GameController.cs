﻿using System;
using ColorLinesEntitas.Features;
using Entitas;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static Contexts SessionContext;
    
    private Systems systems;
    
    void Awake()
    {
        if (!Contexts.sharedInstance.configs.isIsConfigExist)
        {
            Contexts.sharedInstance.configs.SetBoardConfig(ScriptableBoardConfig.Default);
            Contexts.sharedInstance.configs.SetGemsRulesConfig(ScriptableGemsRulesConfig.Default);
            Contexts.sharedInstance.configs.isIsConfigExist = true;
        }

        SessionContext = new Contexts();
        systems = new GameFeature(SessionContext);
        
        systems.Initialize();
    }

    private void Update()
    {
        systems.Execute();
        systems.Cleanup();
    }

    private void OnDestroy()
    {
        systems.DeactivateReactiveSystems();
        systems.TearDown();
        
        SessionContext.Reset();
    }
}
