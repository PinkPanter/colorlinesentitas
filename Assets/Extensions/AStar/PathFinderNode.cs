﻿using UnityEngine;

namespace AStar
{
    public struct PathFinderNode
    {
        public int F_Gone_Plus_Heuristic;
        public int Gone;
        public int Heuristic;  // f = gone + heuristic
        public int X;
        public int Y;
        public int ParentX; // Parent
        public int ParentY;
        
        public Vector2Int Vector2Int => new Vector2Int(X, Y);
    }
}
